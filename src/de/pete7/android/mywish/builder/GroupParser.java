package de.pete7.android.mywish.builder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.pete7.android.mywish.model.Group;
import de.pete7.android.mywish.model.GroupList;
import de.pete7.android.mywish.model.GroupList.InvalidGroupList;
import de.pete7.android.mywish.model.Tag;
import de.pete7.android.mywish.utility.LogUtility;

public class GroupParser {

	private static final String TAG = GroupParser.class.getSimpleName();

	public GroupList parseJson(String json) {
		try {
			GroupList groups = new GroupList();
			JSONArray array = new JSONArray(json);
			for (int i = 0; i < array.length(); i++) {
				JSONObject entry = array.getJSONObject(i);

				Tag newTag = TagBuilder.build(entry);
				Group newGroup = GroupBuilder.build(entry);
				Group group = groups.getGroupById(newGroup.getGroupId());

				if (group == null) {
					group = newGroup;
				}
				groups.removeGroupById(group.getGroupId());
				group.addAnswerOptionTag(newTag);
				groups.addGroup(group);
			}
			return groups;
		} catch (JSONException e) {
			LogUtility.logException(TAG, e);
			return new InvalidGroupList();
		}
	}

}
