package de.pete7.android.mywish.builder;

import org.json.JSONException;
import org.json.JSONObject;

import de.pete7.android.mywish.model.Tag;
import de.pete7.android.mywish.utility.JSONUtility;

public class TagBuilder {
	private static final String AOT_ID = "answerOptionTagId";
	private static final String AOT_LABEL = "label";
	
	public static Tag build(JSONObject object) throws JSONException {
		Tag tag = new Tag();
		tag.setId(JSONUtility.getStringValueFromJSON(object, AOT_ID));
		tag.setLabel(JSONUtility.getStringValueFromJSON(object, AOT_LABEL));
		return tag;
	}
}
