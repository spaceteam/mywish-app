package de.pete7.android.mywish.builder;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.pete7.android.mywish.model.Product;
import de.pete7.android.mywish.utility.JSONUtility;

public class ProductBuilder {
	
	private final static String PRODUCT_LABEL = "title";
	private final static String PRODUCT_DESCRIPTION = "description";
	private final static String PRODUCT_OFFERS = "offers";
	private final static String PRODUCT_OFFERS_PRICE = "displayPrice";
	private final static String PRODUCT_SHOP_ID = "shopId";
	private final static String PRODUCT_SHOP_TITLE = "shopTitle";
	private final static String PRODUCT_LINKS_IMAGE = "image";
	
	//PRODUCT URL
	private final static String PRODUCT_LINKS = "_links";
	private final static String PRODUCT_LINKS_BUY = "buy";
	private final static String HREF = "href";
	
	public static Product buildProduct(JSONArray array) throws JSONException {
		Product product = buildProduct(array.getJSONObject(0), true);
		return product;
	}
	
	public static ArrayList<Product> buildProducts(JSONArray array) throws JSONException {
		ArrayList<Product> list= new ArrayList<Product>();
		for (int i = 0; i < array.length(); i++) {
			Product product = buildProduct(array.getJSONObject(i), false);
			list.add(product);
		}
		return list;
	}
	
	private static Product buildProduct(JSONObject object, boolean oneProduct) throws JSONException {
		Product product = new Product();
		product.setLabel(JSONUtility.getStringValueFromJSON(object, PRODUCT_LABEL));
		
		JSONObject offers = object.getJSONArray(PRODUCT_OFFERS).getJSONObject(0);
		product.setPrice(JSONUtility.getStringValueFromJSON(object, PRODUCT_OFFERS_PRICE));
		product.setShopId(JSONUtility.getStringValueFromJSON(offers, PRODUCT_SHOP_ID));
		product.setShopTitle(JSONUtility.getStringValueFromJSON(offers, PRODUCT_SHOP_TITLE));
		
		//PRODUCT DESCRIPTION / ONLY SOME PRODUCTS HAVE
		if (oneProduct) {
			String description = JSONUtility.getStringValueFromJSON(object, PRODUCT_DESCRIPTION);
			if (description != null) {
				product.setDescription(description);
			}
		}
		
		JSONObject links = object.getJSONObject(PRODUCT_LINKS);
		//IMAGE URL
		if (oneProduct == false) {
			try {
				JSONObject image = links.getJSONObject(PRODUCT_LINKS_IMAGE);
				product.setImageUrl(JSONUtility.getStringValueFromJSON(image, HREF));
			}
			catch (Exception e) {
				e.printStackTrace();
				product.setImageUrl("http://www.darksideapothecary.com/uploads/1/1/4/8/11486524/6676085_orig.jpg");
			}
		}
		
		//PRODUCT URL
		JSONObject buy = links.getJSONObject(PRODUCT_LINKS_BUY);
		product.setShopUrl(JSONUtility.getStringValueFromJSON(buy, HREF));
		return product;
	}
}
