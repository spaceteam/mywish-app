package de.pete7.android.mywish.builder;

import org.json.JSONException;
import org.json.JSONObject;

import de.pete7.android.mywish.model.Category;
import de.pete7.android.mywish.utility.JSONUtility;

public class CategoryBuilder {
	
	private final static String CATEGORY_ID = "categoryId";
	private final static String CATEGORY_LABEL = "label";
	private final static String CATEGORY_DISPLAYPRICE = "displayPrice";
	private final static String CATEGORY_ONE_PRODUCT = "isOneProduct";
	
	//IMAGE URL
	private final static String CATEGORY_LINKS = "_links";
	private final static String CATEGORY_IMAGE_500 = "large";	
	private final static String CATEGORY_HREF = "href";

	public static Category build(JSONObject object) throws JSONException {
		Category category = new Category();
		category.setCategoryId(JSONUtility.getStringValueFromJSON(object, CATEGORY_ID));
		category.setLabel(JSONUtility.getStringValueFromJSON(object, CATEGORY_LABEL));
		category.setDisplayPrice(JSONUtility.getStringValueFromJSON(object, CATEGORY_DISPLAYPRICE));
		category.setOneProduct(JSONUtility.getBooleanValueFromJSON(object, CATEGORY_ONE_PRODUCT));
		
		//IMAGE URL
		JSONObject links = object.getJSONObject(CATEGORY_LINKS);
		JSONObject image = links.getJSONObject(CATEGORY_IMAGE_500);
		category.setImageUrl(JSONUtility.getStringValueFromJSON(image, CATEGORY_HREF));

		return category;
	}
}
