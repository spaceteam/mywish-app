package de.pete7.android.mywish.builder;

import org.json.JSONException;
import org.json.JSONObject;

import de.pete7.android.mywish.model.Group;
import de.pete7.android.mywish.utility.JSONUtility;

public class GroupBuilder {
	
	private static final String AOT_GROUP_ID = "groupId";
	private static final String AOT_GROUP_LABEL = "groupLabel";
	
	public static Group build(JSONObject object) throws JSONException {
		Group group = new Group();
		group.setGroupId(JSONUtility.getStringValueFromJSON(object, AOT_GROUP_ID));
		group.setLabel(JSONUtility.getStringValueFromJSON(object, AOT_GROUP_LABEL));
		return group;
	}
}
