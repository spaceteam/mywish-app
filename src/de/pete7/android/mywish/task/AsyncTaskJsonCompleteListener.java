package de.pete7.android.mywish.task;


public interface AsyncTaskJsonCompleteListener {

	public void onTaskComplete(String result);

}
