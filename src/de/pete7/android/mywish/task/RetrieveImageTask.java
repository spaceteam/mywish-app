package de.pete7.android.mywish.task;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import de.pete7.android.mywish.utility.ImageUtility;

public class RetrieveImageTask extends AsyncTask<String, Integer, Bitmap> {
	
	private AsyncTaskImageCompleteListener listener;
	
	public RetrieveImageTask(AsyncTaskImageCompleteListener listener) {
		this.listener = listener;
	}
	
	protected void onPreExecute() {
		super.onPreExecute();
	}
	
	@Override
	protected Bitmap doInBackground(String... params) {
		return ImageUtility.getBitmapFromURL(params[0]);
	}
	
	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);
		listener.onTaskComplete(result);
	}
}
