package de.pete7.android.mywish.task;

import android.os.AsyncTask;
import de.pete7.android.mywish.utility.JSONUtility;

public class RetrieveJsonTask extends AsyncTask<String, Integer, String> {
	
	private final String TAG = RetrieveJsonTask.class.getSimpleName();	
	private AsyncTaskJsonCompleteListener listener;
	
	public RetrieveJsonTask(AsyncTaskJsonCompleteListener listener) {
		this.listener = listener;
	}
	
	protected void onPreExecute() {
		super.onPreExecute();
	}
	
	@Override
	protected String doInBackground(String... params) {
		return JSONUtility.getJSONStringfrom(params[0], TAG);
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		listener.onTaskComplete(result);
	}
}
