package de.pete7.android.mywish.task;

import android.graphics.Bitmap;


public interface AsyncTaskImageCompleteListener {

	public void onTaskComplete(Bitmap result);

}
