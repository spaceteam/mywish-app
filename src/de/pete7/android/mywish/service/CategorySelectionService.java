package de.pete7.android.mywish.service;

import java.util.ArrayList;

import de.pete7.android.mywish.model.Category;

public class CategorySelectionService {
	private ArrayList<Category> alreadyShownCategories;
	private final ArrayList<Category> retrievedCategories;
	private final ArrayList<Category> visibleCategories;

	public CategorySelectionService(ArrayList<Category> retrievedCategories, ArrayList<Category> alreadyShownCategories) {
		this.retrievedCategories = new ArrayList<Category>(retrievedCategories);
		this.visibleCategories = new ArrayList<Category>(retrievedCategories);
		if (alreadyShownCategories == null) {
			this.alreadyShownCategories = new ArrayList<Category>();
		} else {
			this.alreadyShownCategories = new ArrayList<Category>(alreadyShownCategories);
		}
	}

	public ArrayList<Category> getVisibleCategories() {
		return visibleCategories;
	}

	public ArrayList<Category> getAlreadyShownCategories() {
		ArrayList<Category> newAlreadyShownCategories = new ArrayList<Category>(alreadyShownCategories);
		for (Category r : retrievedCategories) {
			boolean rIsInN = false;
			for (Category n : newAlreadyShownCategories) {
				if (r.getCategoryId().equalsIgnoreCase(n.getCategoryId())) {
					rIsInN = true;
				}
			}
			if (rIsInN == false) {
				newAlreadyShownCategories.add(r);
			}
		}
		return newAlreadyShownCategories;
	}

	public void doSelection() {
		for (Category r : retrievedCategories) {
			for (Category a : alreadyShownCategories) {
				if (r.getCategoryId().equalsIgnoreCase(a.getCategoryId())) {
					int indexToRemove = -1;
					for (int i = 0; i < visibleCategories.size(); i++) {
						Category v = visibleCategories.get(i);
						if (v.getCategoryId().equalsIgnoreCase(r.getCategoryId())) {
							indexToRemove = i;
						}
					}
					visibleCategories.remove(indexToRemove);
				}
			}
		}
	}
}
