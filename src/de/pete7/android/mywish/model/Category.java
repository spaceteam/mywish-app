package de.pete7.android.mywish.model;

import java.io.Serializable;

import android.graphics.Bitmap;

@SuppressWarnings("serial")
public class Category implements Serializable {

	private String categoryId;
	private String label;
	private String displayPrice;
	private String imageUrl;
	private Bitmap image;
	private boolean oneProduct;

	public Category() {
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDisplayPrice() {
		return displayPrice;
	}

	public void setDisplayPrice(String displayPrice) {
		this.displayPrice = displayPrice;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Bitmap getImage() {
		return image;
	}

	public void setImage(Bitmap image) {
		this.image = image;
	}

	public boolean isOneProduct() {
		return oneProduct;
	}

	public void setOneProduct(boolean oneProduct) {
		this.oneProduct = oneProduct;
	}

	@Override
	public String toString() {
		return label;
	}
}
