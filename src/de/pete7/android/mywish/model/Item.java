package de.pete7.android.mywish.model;

import android.graphics.Bitmap;

public abstract class Item {
	
	public abstract String getId();
	public abstract void setId(String id);
	public abstract String getImageUrl();
	public abstract void setImageUrl(String imageUrl);
	public abstract Bitmap getImage();
	public abstract void setImage(Bitmap image);
	
}
