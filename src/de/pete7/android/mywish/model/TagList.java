package de.pete7.android.mywish.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("serial")
public class TagList implements Serializable {

	private final ArrayList<Tag> tags;

	public TagList() {
		tags = new ArrayList<Tag>();
	}

	public void addTag(Tag tag) {
		tags.add(tag);
	}

	public void addAll(Collection<? extends Tag> collection) {
		tags.addAll(collection);
	}

	public Tag getTagById(String tagId) {
		for (Tag tag : tags) {
			if (tagId.equalsIgnoreCase(tag.getId())) {
				return tag;
			}
		}
		return null;
	}

	public Tag getTagByIndex(int index) {
		return tags.get(index);
	}

	public ArrayList<String> getAllIds() {
		ArrayList<String> strings = new ArrayList<String>();
		for (Tag tag : tags) {
			strings.add(tag.getId());
		}
		return strings;
	}

	public ArrayList<String> getAllLabels() {
		ArrayList<String> labels = new ArrayList<String>();
		for (Tag tag : tags) {
			labels.add(tag.getLabel());
		}
		return labels;
	}

	public ArrayList<Tag> getAllTags() {
		return tags;
	}

	public int getSize() {
		return tags.size();
	}

}
