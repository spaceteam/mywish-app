package de.pete7.android.mywish.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("serial")
public class GroupList implements Serializable {

	public static class InvalidGroupList extends GroupList {
		@Override
		public boolean isValid() {
			return false;
		}
	}

	private final ArrayList<Group> list;

	public GroupList() {
		list = new ArrayList<Group>();
	}

	public void addGroup(Group group) {
		list.add(group);
	}

	public boolean isValid() {
		return true;
	}

	public Group getGroupById(String groupId) {
		Group group = null;
		for (Group g : list) {
			if (g.getGroupId().equalsIgnoreCase(groupId)) {
				group = g;
			}
		}
		return group;
	}

	public void removeGroupById(String groupId) {
		Group groupToRemove = null;
		for (Group g : list) {
			if (g.getGroupId().equalsIgnoreCase(groupId)) {
				groupToRemove = g;
			}
		}
		list.remove(groupToRemove);
	}

	public void removeGroupByIndex(int index) {
		list.remove(index);
	}

	public void addAll(Collection<? extends Group> collection) {
		list.addAll(collection);
	}

	public ArrayList<Group> getList() {
		return list;
	}

}
