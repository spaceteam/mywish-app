package de.pete7.android.mywish.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Tag implements Serializable {

	private String id;
	private String label;

	public Tag() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return label;
	}
}
