package de.pete7.android.mywish.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Group implements Serializable {

	private String groupId;
	private String groupLabel;
	private final TagList tags;

	public Group() {
		tags = new TagList();
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getLabel() {
		return groupLabel;
	}

	public void setLabel(String label) {
		this.groupLabel = label;
	}

	public void addAnswerOptionTag(Tag tag) {
		tags.addTag(tag);
	}

	public void removeTag(int index) {
		tags.getAllTags().remove(index);
	}

	public TagList getTagList() {
		return tags;
	}

	public Tag findAnswerOptionTagById(String id) {
		return tags.getTagById(id);
	}

}
