package de.pete7.android.mywish.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Person implements Serializable {
	private final String name;
	private boolean male;

	public Person(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setMale(boolean male) {
		this.male = male;
	}

	public boolean isMale() {
		return male;
	}
}
