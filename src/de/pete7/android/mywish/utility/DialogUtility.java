package de.pete7.android.mywish.utility;

import android.app.AlertDialog;
import android.content.Context;

public class DialogUtility {
	public static void buildDialog(Context context, String title, String msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setPositiveButton(android.R.string.ok, null);
		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
