package de.pete7.android.mywish.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import android.util.Log;

public class JSONUtility {
	
	public static String getStringValueFromJSON(JSONObject object, String key){
		String value = "";
		try {
			value = object.getString(key);
		} catch (Exception e) {
			e.printStackTrace();
		}  
		return value;
	}
	
	public static Boolean getBooleanValueFromJSON(JSONObject object, String key){
		Boolean value = null;
		try {
			value = object.getBoolean(key);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return value;
	}
	
	public static String getJSONStringfrom(String url, String tag) {
		String responseData = null;
		
		try {
			URL apiUrl = new URL(url);
						
			HttpURLConnection connection = 
					(HttpURLConnection) apiUrl.openConnection();
			connection.connect();
			int responseCode = connection.getResponseCode();
			
			if (responseCode == HttpURLConnection.HTTP_OK) {
				//getting the root-JSON Object
				InputStream in = connection.getInputStream();
				responseData = getStringFromInputStream(in);
			}
			
			else {
				Log.i(tag, "Unsuccessful HTTP Response Code:" + responseCode);
			}
		}
		catch (IOException e) {
			LogUtility.logException(tag, e);
		}
		catch (Exception e) {
			LogUtility.logException(tag, e);
		}

		return responseData;
	}
	
	private static String getStringFromInputStream(InputStream is) {
		 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
 
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
		return sb.toString();
 
	}
}
