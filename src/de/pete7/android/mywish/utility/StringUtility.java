package de.pete7.android.mywish.utility;

import java.util.ArrayList;

import de.pete7.android.mywish.R;

import android.content.Context;

public class StringUtility {
	
	public static String buildTitle(Context context, int titleFrag, String name) {
		StringBuilder question = new StringBuilder();
		question.append(context.getString(titleFrag));
		question.append(" ");
		question.append(name);
		return question.toString();
	}
	
	public static String buildQuestionWithName(Context context, int questionFrag, String name) {
		StringBuilder question = new StringBuilder();
		question.append(context.getString(questionFrag));
		question.append(" ");
		question.append(name);
		question.append("?");
		return question.toString();
	}
	
	public static String buildQuestionWithName(Context context, int questionFrag1, String name, int questionFrag2) {
		StringBuilder question = new StringBuilder();
		question.append(context.getString(questionFrag1));
		question.append(" ");
		question.append(name);
		question.append(" ");
		question.append(context.getString(questionFrag2));
		question.append("?");
		return question.toString();
	}
	
	public static String buildQuestionWithGender(Context context, int questionFrag, boolean male) {
		StringBuilder question = new StringBuilder();
		question.append(context.getString(questionFrag));
		question.append(" ");
		if (male) {
			question.append(context.getString(R.string.male));
		}
		else {
			question.append(context.getString(R.string.female));
		}
		question.append("?");
		return question.toString();
	}
	
	public static String buildBasicString(String baseUrl, String locationUrl) {
		StringBuilder url = initializeWithBaseUrl(baseUrl);
		url.append(locationUrl);
		return url.toString();
	}
	
	public static String buildCombinedTagString(String baseUrl, String locationUrl, ArrayList<String> ids) {
		StringBuilder url = initializeWithBaseUrl(baseUrl);
		url.append(locationUrl);
		url.append("?");
		appendTagsFromArray(ids, url);
		return url.toString();
	}
	
	public static String buildRelatedTagsUrl(String baseUrl, String locationUrl, ArrayList<String> ids, String tagAppendix) {
		StringBuilder url = initializeWithBaseUrl(baseUrl);
		url.append(locationUrl);
		url.append("?");
		appendTagsFromArray(ids, url);
		url.append("/");
		url.append(tagAppendix);
		return url.toString();
	}
	
	public static String buildProductsUrl(String baseUrl, String directCategoryUrl, String categoryId, String productsAppendix) {
		StringBuilder url = initializeWithBaseUrl(baseUrl);
		url.append(directCategoryUrl);
		url.append("/");
		url.append(categoryId);
		url.append("/");
		url.append(productsAppendix);
		return url.toString();
	}
	
	private static StringBuilder initializeWithBaseUrl(String baseUrl) {
		StringBuilder url = new StringBuilder();
		url.append(baseUrl);
		return url;
	}
	
	private static void appendTagsFromArray(ArrayList<String> ids,
			StringBuilder url) {
		for (String id : ids) {
			if(id != ids.get(0)) {
				url.append("&");
			}
			url.append("tag=" + id); 
		}
	}
}
