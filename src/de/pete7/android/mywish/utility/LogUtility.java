package de.pete7.android.mywish.utility;

import android.util.Log;

public class LogUtility {
	public static void logException(String TAG, Exception e) {
		Log.e(TAG, e.toString());
	}
	
	public static void logInfo(String TAG, String info) {
		Log.i(TAG, info);
	}
}
