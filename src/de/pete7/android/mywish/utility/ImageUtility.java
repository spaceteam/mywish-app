package de.pete7.android.mywish.utility;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ImageUtility {
	public static Bitmap getBitmapFromURL(String bitmapUrl) {
		try {
			URL url = new URL(bitmapUrl);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inSampleSize = 2;
			Bitmap image = BitmapFactory.decodeStream(input, null, o);
			return image;
		} 
		
		catch (IOException e) {
			LogUtility.logException("ImageUtility", e);
			e.printStackTrace();
			return null;
		}
	}
}
