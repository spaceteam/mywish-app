package de.pete7.android.mywish.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import de.pete7.android.mywish.R;
import de.pete7.android.mywish.model.Product;

public class MultiProductViewGridListAdapter extends BaseAdapter {

	private ArrayList<Product> products;
	private LayoutInflater inflater;
	
	public MultiProductViewGridListAdapter(Context context, ArrayList<Product> products) {
		this.products = products;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return products.size();
	}

	@Override
	public Object getItem(int index) {
		return products.get(index);
	}

	@Override
	public long getItemId(int index) {
		return 0;
	}

	@Override
	public View getView(int index, View view, ViewGroup parent) {
		if (view == null) {
			view = inflater.inflate(R.layout.multi_product_view_list_item, parent, false);
			view.setTag(R.id.multi_product_view_item_picture, view.findViewById(R.id.multi_product_view_item_picture));
			view.setTag(R.id.multi_product_view_item_text, view.findViewById(R.id.multi_product_view_item_text));
			view.setTag(R.id.multi_product_view_item_price, view.findViewById(R.id.multi_product_view_item_price));
		}
		
		ImageView picture = (ImageView) view.getTag(R.id.multi_product_view_item_picture);
		TextView text = (TextView) view.getTag(R.id.multi_product_view_item_text);
		TextView price = (TextView) view.getTag(R.id.multi_product_view_item_price);
		
		picture.setImageBitmap(products.get(index).getImage());
		text.setText(products.get(index).getLabel());
		price.setText(products.get(index).getPrice());

		return view;
	}
}
