package de.pete7.android.mywish.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import de.pete7.android.mywish.model.Tag;

public class PrologListAdapter extends ArrayAdapter<Tag> {
	private final Context context;
	private final int resourceId;
	private final int itemId;

	public PrologListAdapter(Context context, int resourceId, int itemId, ArrayList<Tag> list) {
		super(context, resourceId, list);
		this.context = context;
		this.resourceId = resourceId;
		this.itemId = itemId;
	}

	@Override
	public View getView(int index, View view, ViewGroup parent) {
		if (view == null) {
			view = View.inflate(context, resourceId, null);
		}

		Tag tag = getItem(index);
		if (tag != null) {
			TextView itemLabel = (TextView) view.findViewById(itemId);
			itemLabel.setText(getItem(index).getLabel());
		}
		return view;
	}

}
