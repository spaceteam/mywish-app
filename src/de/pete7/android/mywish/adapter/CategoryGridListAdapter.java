package de.pete7.android.mywish.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import de.pete7.android.mywish.R;
import de.pete7.android.mywish.model.Category;

public class CategoryGridListAdapter extends BaseAdapter {

	private final ArrayList<Category> categories;
	private final LayoutInflater inflater;
	private final Context context;

	public CategoryGridListAdapter(Context context, ArrayList<Category> categories) {
		this.categories = categories;
		this.context = context;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return categories.size() + 1;
	}

	@Override
	public Object getItem(int index) {
		return categories.get(index);
	}

	@Override
	public long getItemId(int index) {
		return 0;
	}

	@Override
	public View getView(int index, View view, ViewGroup parent) {
		if (view == null) {
			view = inflater.inflate(R.layout.category_list_item, parent, false);
			view.setTag(R.id.product_list_item_picture, view.findViewById(R.id.product_list_item_picture));
			view.setTag(R.id.product_list_item_text, view.findViewById(R.id.product_list_item_text));
			view.setTag(R.id.product_list_item_price, view.findViewById(R.id.product_list_item_price));
		}

		ImageView picture = (ImageView) view.getTag(R.id.product_list_item_picture);
		TextView text = (TextView) view.getTag(R.id.product_list_item_text);
		TextView price = (TextView) view.getTag(R.id.product_list_item_price);

		if (isLastElement(index)) {
			picture.setImageResource(R.drawable.ic_action_new_label);
			text.setText(context.getString(R.string.category_list_related_tags_description));
			price.setText("");
			return view;
		}

		picture.setImageBitmap(categories.get(index).getImage());
		text.setText(categories.get(index).getLabel());
		price.setText(categories.get(index).getDisplayPrice());

		return view;
	}

	private boolean isLastElement(int index) {
		return index == getCount() - 1;
	}

}
