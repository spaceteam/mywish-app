package de.pete7.android.mywish.activity;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import de.pete7.android.mywish.R;
import de.pete7.android.mywish.builder.ProductBuilder;
import de.pete7.android.mywish.model.Category;
import de.pete7.android.mywish.model.Product;
import de.pete7.android.mywish.task.AsyncTaskImageCompleteListener;
import de.pete7.android.mywish.task.AsyncTaskJsonCompleteListener;
import de.pete7.android.mywish.task.RetrieveImageTask;
import de.pete7.android.mywish.task.RetrieveJsonTask;
import de.pete7.android.mywish.utility.StringUtility;

public class OneProductViewActivity extends Activity implements OnClickListener {

	public static final String TAG = OneProductViewActivity.class.getSimpleName();

	private Category category;
	private Product product;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_one_product_view);

		fetchDataFromIntent();
		retrieveProductFromRemote();
	}

	private void prepareActionBar() {
		TextView title = (TextView) findViewById(R.id.action_bar_title);
		title.setText(product.getLabel());
	}

	private void fetchDataFromIntent() {
		Bundle data = getIntent().getExtras();
		category = (Category) data.getSerializable(getString(R.string.intent_product_view_category));
	}

	private void retrieveProductFromRemote() {
		String productUrl = StringUtility.buildProductsUrl(getString(R.string.base_url),
				getString(R.string.direct_categories_url), category.getCategoryId(),
				getString(R.string.products_appendix));
		new RetrieveJsonTask(new RetrieveProductTaskCompleteListener()).execute(productUrl);
	}

	private void retrieveImageFromRemote() {
		new RetrieveImageTask(new RetrieveImageTaskCompleteListener()).execute(category.getImageUrl());
	}

	private void prepareViewItems() {
		prepareActionBar();

		TextView description = (TextView) findViewById(R.id.one_product_view_category_description);
		description.setText(product.getDescription());

		Button buyButton = (Button) findViewById(R.id.one_product_view_buy_button);
		buyButton.setOnClickListener(this);
	}

	private class RetrieveProductTaskCompleteListener implements AsyncTaskJsonCompleteListener {
		@Override
		public void onTaskComplete(String result) {
			JSONArray array;
			try {
				array = new JSONArray(result);
				product = ProductBuilder.buildProduct(array);
				prepareViewItems();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			retrieveImageFromRemote();
		}
	}

	private class RetrieveImageTaskCompleteListener implements AsyncTaskImageCompleteListener {
		@Override
		public void onTaskComplete(Bitmap result) {
			product.setImage(result);
			prepareImageView();
		}
	}

	private void prepareImageView() {
		ImageView image = (ImageView) findViewById(R.id.one_product_view_image);
		image.setImageBitmap(product.getImage());

		ProgressBar loading = (ProgressBar) findViewById(R.id.one_product_view_progress_bar);
		loading.setVisibility(View.GONE);
	}

	@Override
	public void onClick(View view) {
		Intent toShop = new Intent(Intent.ACTION_VIEW, Uri.parse(product.getShopUrl()));
		startActivity(toShop);
	}
}
