package de.pete7.android.mywish.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import de.pete7.android.mywish.R;
import de.pete7.android.mywish.adapter.MultiProductViewGridListAdapter;
import de.pete7.android.mywish.builder.ProductBuilder;
import de.pete7.android.mywish.model.Category;
import de.pete7.android.mywish.model.Product;
import de.pete7.android.mywish.task.AsyncTaskJsonCompleteListener;
import de.pete7.android.mywish.task.RetrieveJsonTask;
import de.pete7.android.mywish.utility.ImageUtility;
import de.pete7.android.mywish.utility.StringUtility;

public class MultiProductViewActivity extends Activity {

	public static final String TAG = MultiProductViewActivity.class.getSimpleName();

	private Category category;
	private ArrayList<Product> productList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multi_product_view);
		fetchDataFromIntent();
		retrieveProductsFromRemote();
	}

	private void fetchDataFromIntent() {
		Bundle data = getIntent().getExtras();
		category = (Category) data.getSerializable(getString(R.string.intent_product_view_category));
	}

	private void prepareActionBar() {
		TextView title = (TextView) findViewById(R.id.action_bar_title);
		title.setText(category.getLabel());
	}

	private void retrieveProductsFromRemote() {
		String productUrl = StringUtility.buildProductsUrl(getString(R.string.base_url),
				getString(R.string.direct_categories_url), category.getCategoryId(),
				getString(R.string.products_appendix));
		new RetrieveJsonTask(new RetrieveProductsTaskCompleteListener()).execute(productUrl);
	}

	private void retrieveImagesFromRemote() {
		RetrieveImagesTask retrieveImagesTask = new RetrieveImagesTask();
		retrieveImagesTask.execute(productList);
	}

	private class RetrieveProductsTaskCompleteListener implements AsyncTaskJsonCompleteListener {

		@Override
		public void onTaskComplete(String result) {
			try {
				JSONArray array = new JSONArray(result);
				productList = ProductBuilder.buildProducts(array);
				retrieveImagesFromRemote();
				prepareActionBar();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private class RetrieveImagesTask extends AsyncTask<ArrayList<Product>, Void, ArrayList<Product>> {

		@Override
		protected ArrayList<Product> doInBackground(ArrayList<Product>... prods) {
			ArrayList<Product> products = new ArrayList<Product>(prods[0]);
			for (Product p : products) {
				p.setImage(ImageUtility.getBitmapFromURL(p.getImageUrl()));
			}
			return products;
		}

		@Override
		protected void onPostExecute(ArrayList<Product> result) {
			super.onPostExecute(result);
			productList = new ArrayList<Product>(result);
			prepareViewItems();
		}
	}

	private void prepareViewItems() {
		MultiProductViewGridListAdapter ia = new MultiProductViewGridListAdapter(this, productList);
		GridView gridView = (GridView) findViewById(R.id.multi_product_view_grid);
		gridView.setAdapter(ia);
		prepareOnClickItemListener(gridView);
		hideProgressBar();
	}

	private void prepareOnClickItemListener(GridView gridView) {
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String shopUrl = productList.get(position).getShopUrl();
				Intent toShop = new Intent(Intent.ACTION_VIEW, Uri.parse(shopUrl));
				startActivity(toShop);
			}
		});
	}

	private void hideProgressBar() {
		ProgressBar pg = (ProgressBar) findViewById(R.id.multi_product_view_progress_bar);
		pg.setVisibility(View.GONE);
	}
}
