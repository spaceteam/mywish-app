package de.pete7.android.mywish.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import de.pete7.android.mywish.R;
import de.pete7.android.mywish.adapter.CategoryGridListAdapter;
import de.pete7.android.mywish.builder.CategoryBuilder;
import de.pete7.android.mywish.builder.TagBuilder;
import de.pete7.android.mywish.model.Category;
import de.pete7.android.mywish.model.Person;
import de.pete7.android.mywish.model.Tag;
import de.pete7.android.mywish.model.TagList;
import de.pete7.android.mywish.service.CategorySelectionService;
import de.pete7.android.mywish.task.AsyncTaskJsonCompleteListener;
import de.pete7.android.mywish.task.RetrieveJsonTask;
import de.pete7.android.mywish.utility.ImageUtility;
import de.pete7.android.mywish.utility.LogUtility;
import de.pete7.android.mywish.utility.StringUtility;

public class CategoryListActivity extends Activity {

	public static final String TAG = CategoryListActivity.class.getSimpleName();

	private TagList choices;
	private TagList relatedTags;
	private ArrayList<Category> visibleCategories;
	private ArrayList<Category> alreadyShownCategories;
	private Person person;

	private boolean firstShownList = false;

	private DrawerLayout rightDrawerLayout;
	private ActionBarDrawerToggle rightDrawerToggle;
	private boolean drawerOpen = false;
	private ListView relatedTagsList;

	private String title;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category_list);
		checkIfFirstShownList();
		fetchDataFromIntent();
		prepareActionBar();
		prepareDrawer();
		retrieveCategoriesFromRemote();
	}

	private void checkIfFirstShownList() {
		String senderActivity = getCallingActivity().getClassName();
		String prologClass = PrologOccasionActivity.class.getName();
		if (senderActivity.equalsIgnoreCase(prologClass)) {
			firstShownList = true;
		}
	}

	@Override
	public void onBackPressed() {
		if (firstShownList) {
			Toast noBack = Toast.makeText(this, getString(R.string.no_back_action), Toast.LENGTH_LONG);
			noBack.show();
		} else {
			super.onBackPressed();
		}
	}

	private void fetchDataFromIntent() {
		Bundle fromIntent = getIntent().getExtras();
		choices = (TagList) fromIntent.getSerializable(getString(R.string.intent_choices));
		alreadyShownCategories = (ArrayList<Category>) fromIntent
				.getSerializable(getString(R.string.intent_product_list_categories_to_self));
		person = (Person) fromIntent.getSerializable(getString(R.string.intent_person));
	}

	private void prepareActionBar() {
		TextView titleLabel = (TextView) findViewById(R.id.action_bar_title);
		title = StringUtility.buildTitle(this, R.string.category_list_title, person.getName());
		titleLabel.setText(title);
	}

	private void retrieveCategoriesFromRemote() {
		String productUrl = StringUtility.buildCombinedTagString(getString(R.string.base_url),
				getString(R.string.categories_url), choices.getAllIds());
		new RetrieveJsonTask(new RetrieveCategoriesTaskCompleteListener()).execute(productUrl);
	}

	private class RetrieveCategoriesTaskCompleteListener implements AsyncTaskJsonCompleteListener {

		@Override
		public void onTaskComplete(String result) {
			ArrayList<Category> retrievedCategories = new ArrayList<Category>();
			retrievedCategories = parseJSONToCategoryList(result);
			categorySelection(retrievedCategories);
			retrieveImagesFromRemote();
		}
	}

	private ArrayList<Category> parseJSONToCategoryList(String json) {
		ArrayList<Category> retrievedCategories = null;
		try {

			JSONArray array = new JSONArray(json);
			retrievedCategories = new ArrayList<Category>();
			for (int i = 0; i < array.length(); i++) {
				JSONObject jsonCategory = array.getJSONObject(i);
				Category currentCategory = CategoryBuilder.build(jsonCategory);
				retrievedCategories.add(currentCategory);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return retrievedCategories;
	}

	private void categorySelection(ArrayList<Category> retrievedCategories) {
		CategorySelectionService service = new CategorySelectionService(retrievedCategories, alreadyShownCategories);
		service.doSelection();
		alreadyShownCategories = new ArrayList<Category>(service.getAlreadyShownCategories());
		visibleCategories = new ArrayList<Category>(service.getVisibleCategories());
	}

	private void retrieveImagesFromRemote() {
		RetrieveImagesTask retrieveImagesTask = new RetrieveImagesTask();
		retrieveImagesTask.execute(visibleCategories);
	}

	private class RetrieveImagesTask extends AsyncTask<ArrayList<Category>, Void, ArrayList<Category>> {

		@Override
		protected ArrayList<Category> doInBackground(ArrayList<Category>... cats) {
			ArrayList<Category> categories = new ArrayList<Category>(cats[0]);
			for (Category c : categories) {
				c.setImage(ImageUtility.getBitmapFromURL(c.getImageUrl()));
			}
			return categories;
		}

		@Override
		protected void onPostExecute(ArrayList<Category> result) {
			super.onPostExecute(result);
			visibleCategories = new ArrayList<Category>(result);
			if (visibleCategories.size() > 0) {
				prepareProductList();
			} else {
				showNoItemsText();
			}
			hideProgressBar();
			retrieveRelatedTagsFromRemote();
		}

		private void showNoItemsText() {
			TextView noItems = (TextView) findViewById(R.id.category_list_no_items);
			noItems.setVisibility(View.VISIBLE);
		}
	}

	private void retrieveRelatedTagsFromRemote() {
		String tagsUrl = StringUtility.buildCombinedTagString(getString(R.string.base_url),
				getString(R.string.related_targetgroups_url), choices.getAllIds());
		new RetrieveJsonTask(new RetrieveTagsTaskCompleteListener()).execute(tagsUrl);
	}

	private class RetrieveTagsTaskCompleteListener implements AsyncTaskJsonCompleteListener {

		@Override
		public void onTaskComplete(String result) {
			parseJSONToRelatedTags(result);
			addListToDrawer();
		}
	}

	private void parseJSONToRelatedTags(String json) {
		JSONArray array = null;
		if (relatedTags == null) {
			relatedTags = new TagList();
		}
		try {
			array = new JSONObject(json).getJSONArray(getString(R.string.related_tags_tags));
			for (int i = 0; i < array.length(); i++) {
				JSONObject entry = array.getJSONObject(i);
				Tag currentAot = TagBuilder.build(entry);
				relatedTags.addTag(currentAot);
			}
		} catch (JSONException e) {
			LogUtility.logException(TAG, e);
		}
	}

	private void addListToDrawer() {
		relatedTagsList
				.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, relatedTags.getAllLabels()));
		relatedTagsList.setOnItemClickListener(new DrawerItemClickListener());
	}

	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			addItemToChoiceList(position);
			gotoSelf();
		}
	}

	private void prepareProductList() {
		CategoryGridListAdapter ia = new CategoryGridListAdapter(this, visibleCategories);
		GridView gridView = (GridView) findViewById(R.id.category_list_gridview);
		gridView.setAdapter(ia);
		gridView.setVisibility(View.VISIBLE);
		prepareOnClickItemProductList(gridView);
	}

	private void hideProgressBar() {
		ProgressBar pg = (ProgressBar) findViewById(R.id.category_list_progress_bar);
		pg.setVisibility(View.GONE);
	}

	private void prepareOnClickItemProductList(GridView gridView) {
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				int lastItem = parent.getCount() - 1;
				if (position == lastItem) {
					rightDrawerLayout.openDrawer(relatedTagsList);
				} else {
					gotoProductView(position);
				}
			}
		});
	}

	private void gotoProductView(long categoryId) {
		Category selectedCategory = visibleCategories.get((int) categoryId);
		Intent toProductView;
		if (selectedCategory.isOneProduct()) {
			toProductView = new Intent(this, OneProductViewActivity.class);
		} else {
			toProductView = new Intent(this, MultiProductViewActivity.class);
		}
		addDataForProductView(selectedCategory, toProductView);
		startActivity(toProductView);
	}

	private void addDataForProductView(Category selectedCategory, Intent toProductView) {
		Bundle data = new Bundle();
		selectedCategory.setImage(null);
		data.putSerializable(getString(R.string.intent_product_view_category), selectedCategory);
		toProductView.putExtras(data);
	}

	private void gotoSelf() {
		Intent toSelf = new Intent(this, CategoryListActivity.class);
		addDataForSelf(toSelf);
		toSelf.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		startActivityForResult(toSelf, 0);
		rightDrawerLayout.closeDrawers();
	}

	private void addDataForSelf(Intent toSelf) {
		Bundle data = new Bundle();
		for (Category c : alreadyShownCategories) {
			c.setImage(null);
		}
		data.putSerializable(getString(R.string.intent_product_list_categories_to_self), alreadyShownCategories);
		data.putSerializable(getString(R.string.intent_choices), choices);
		data.putSerializable(getString(R.string.intent_person), person);
		toSelf.putExtras(data);
	}

	private void addItemToChoiceList(long id) {
		Tag currentAOT = relatedTags.getTagByIndex((int) id);
		choices.addTag(currentAOT);
	}

	private void prepareDrawer() {
		rightDrawerLayout = (DrawerLayout) findViewById(R.id.category_list_right_drawer);
		relatedTagsList = (ListView) findViewById(R.id.category_list_related_tags_list);
		rightDrawerToggle = new ActionBarDrawerToggle(this, rightDrawerLayout, R.drawable.ic_action_new_label, 0, 0) {
			TextView titleLabel = (TextView) findViewById(R.id.action_bar_title);

			@Override
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				titleLabel.setText(title);
				drawerOpen = false;
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				titleLabel.setText(buildDrawerTitle());
				drawerOpen = true;
			}
		};
		rightDrawerLayout.setDrawerListener(rightDrawerToggle);
	}

	private String buildDrawerTitle() {
		return StringUtility.buildQuestionWithName(this, R.string.category_list_related_tags_title, person.getName());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.category_list_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.category_list_drawer_action:
			if (drawerOpen) {
				rightDrawerLayout.closeDrawers();
			} else {
				rightDrawerLayout.openDrawer(relatedTagsList);
			}
			return true;
		case R.id.restart_action:
			Intent toHomescreen = new Intent(this, HomescreenActivity.class);
			startActivity(toHomescreen);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
