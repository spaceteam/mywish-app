package de.pete7.android.mywish.activity;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import de.pete7.android.mywish.R;
import de.pete7.android.mywish.builder.GroupParser;
import de.pete7.android.mywish.model.GroupList;
import de.pete7.android.mywish.model.Person;
import de.pete7.android.mywish.model.TagList;
import de.pete7.android.mywish.task.AsyncTaskJsonCompleteListener;
import de.pete7.android.mywish.task.RetrieveJsonTask;
import de.pete7.android.mywish.utility.StringUtility;

public class HomescreenActivity extends Activity {

	public static final String TAG = HomescreenActivity.class.getSimpleName();

	private GroupList groups = new GroupList();
	private Person person;
	private final TagList choices = new TagList();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homescreen);
		prepareActionBar();
		prepareGoButton();
		retrieveDataFromRemote();
	}

	private void prepareActionBar() {
		((TextView) findViewById(R.id.action_bar_title)).setText(R.string.homescreen_title);
	}

	private void prepareGoButton() {
		((Button) findViewById(R.id.homescreen_go_button)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				person = createPerson();
				Intent toPrologGender = new Intent(getApplicationContext(), PrologGenderActivity.class);
				toPrologGender.putExtras(createBundle());
				toPrologGender.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivity(toPrologGender);
			}

			private Person createPerson() {
				EditText name = (EditText) findViewById(R.id.homescreen_name_edittext);
				return new Person(name.getText().toString().trim());
			}

			private Bundle createBundle() {
				Bundle data = new Bundle();
				data.putSerializable(getString(R.string.intent_groups), groups);
				data.putSerializable(getString(R.string.intent_choices), choices);
				data.putSerializable(getString(R.string.intent_person), person);
				return data;
			}
		});
	}

	private void retrieveDataFromRemote() {
		String url = StringUtility.buildBasicString(getString(R.string.base_url),
				getString(R.string.answer_option_tags_url));
		new RetrieveJsonTask(new RetrieveJsonTaskCompleteListener()).execute(url);
	}

	private class RetrieveJsonTaskCompleteListener implements AsyncTaskJsonCompleteListener {

		@Override
		public void onTaskComplete(String result) {
			groups = new GroupParser().parseJson(result);
			if (groups.isValid()) {
				hideProgressBar();
				enableGoButton();
			}
		}

		private void hideProgressBar() {
			((ProgressBar) findViewById(R.id.homescreen_progress_bar)).setVisibility(GONE);
		}

		private void enableGoButton() {
			((Button) findViewById(R.id.homescreen_go_button)).setVisibility(VISIBLE);
		}
	}

}
