package de.pete7.android.mywish.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;
import de.pete7.android.mywish.R;
import de.pete7.android.mywish.model.GroupList;
import de.pete7.android.mywish.model.Person;
import de.pete7.android.mywish.model.TagList;
import de.pete7.android.mywish.utility.StringUtility;

public class PrologGenderActivity extends Activity implements OnClickListener {

	public static final String TAG = PrologGenderActivity.class.getSimpleName();

	private GroupList groups;
	private TagList choices;
	private Person person;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prolog_gender);
		fetchDataFromIntent();
		prepareActionBar();
		prepareButtons();
	}

	private void prepareActionBar() {
		TextView title = (TextView) findViewById(R.id.action_bar_title);
		String titleString = StringUtility.buildQuestionWithName(this, R.string.prolog_gender_title, person.getName());
		title.setText(titleString);
	}

	private void fetchDataFromIntent() {
		Bundle fromHomescreen = getIntent().getExtras();
		groups = (GroupList) fromHomescreen.getSerializable(getString(R.string.intent_groups));
		choices = (TagList) fromHomescreen.getSerializable(getString(R.string.intent_choices));
		person = (Person) fromHomescreen.getSerializable(getString(R.string.intent_person));
	}

	private void prepareButtons() {
		FrameLayout maleButton = (FrameLayout) findViewById(R.id.prolog_gender_male_button);
		FrameLayout femaleButton = (FrameLayout) findViewById(R.id.prolog_gender_female_button);
		maleButton.setOnClickListener(this);
		femaleButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.prolog_gender_male_button) {
			groups.removeGroupById(getString(R.string.group_id_female));
			person.setMale(true);
		}
		if (view.getId() == R.id.prolog_gender_female_button) {
			groups.removeGroupById(getString(R.string.group_id_male));
			person.setMale(false);
		}
		gotoPrologAge();
	}

	private void gotoPrologAge() {
		Intent toPrologAge = new Intent(this, PrologAgeActivity.class);
		addDataToIntent(toPrologAge);
		toPrologAge.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		startActivity(toPrologAge);
		finish();
		overridePendingTransition(0, 0);
	}

	private void addDataToIntent(Intent toPrologAge) {
		Bundle data = new Bundle();
		data.putSerializable(getString(R.string.intent_groups), groups);
		data.putSerializable(getString(R.string.intent_choices), choices);
		data.putSerializable(getString(R.string.intent_person), person);
		toPrologAge.putExtras(data);
	}

}
