package de.pete7.android.mywish.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import de.pete7.android.mywish.R;
import de.pete7.android.mywish.adapter.PrologListAdapter;
import de.pete7.android.mywish.model.Group;
import de.pete7.android.mywish.model.GroupList;
import de.pete7.android.mywish.model.Person;
import de.pete7.android.mywish.model.Tag;
import de.pete7.android.mywish.model.TagList;
import de.pete7.android.mywish.utility.StringUtility;

public class PrologRelationActivity extends Activity {

	private GroupList groups;
	private Group relation;
	private TagList choices;
	private Person person;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prolog_relation);
		fetchDataFromIntent();
		prepareActionBar();
		setActiveGroup();
		prepareList();
	}

	private void fetchDataFromIntent() {
		Bundle fromPrologAge = getIntent().getExtras();
		groups = (GroupList) fromPrologAge.getSerializable(getString(R.string.intent_groups));
		choices = (TagList) fromPrologAge.getSerializable(getString(R.string.intent_choices));
		person = (Person) fromPrologAge.getSerializable(getString(R.string.intent_person));
	}

	private void prepareActionBar() {
		TextView title = (TextView) findViewById(R.id.action_bar_title);
		String titleString = StringUtility.buildQuestionWithName(this, R.string.prolog_relation_title1,
				person.getName(), R.string.prolog_relation_title2);
		title.setText(titleString);
	}

	private void setActiveGroup() {
		Group female = groups.getGroupById(getString(R.string.group_id_female));
		Group male = groups.getGroupById(getString(R.string.group_id_male));
		if (female != null) {
			relation = female;
		} else if (male != null) {
			relation = male;
		}

	}

	private void prepareList() {
		ArrayList<Tag> tags = relation.getTagList().getAllTags();
		PrologListAdapter adapter = new PrologListAdapter(this, R.layout.prolog_list_item, R.id.prolog_list_item_label,
				tags);
		ListView list = (ListView) findViewById(R.id.prolog_relation_list);
		list.setAdapter(adapter);
		prepareOnClickItem(list);
	}

	private void prepareOnClickItem(ListView listView) {
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int index, long id) {
				addItemToChoiceList(index);
				gotoPrologOccasion();
			}
		});
	}

	private void addItemToChoiceList(long index) {
		Tag tag = relation.getTagList().getTagByIndex((int) index);
		choices.addTag(tag);
	}

	private void gotoPrologOccasion() {
		Intent toPrologOccasion = new Intent(this, PrologOccasionActivity.class);
		addDataToIntent(toPrologOccasion);
		toPrologOccasion.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		startActivity(toPrologOccasion);
		finish();
		overridePendingTransition(0, 0);
	}

	private void addDataToIntent(Intent toPrologRelation) {
		Bundle data = new Bundle();
		data.putSerializable(getString(R.string.intent_groups), groups);
		data.putSerializable(getString(R.string.intent_choices), choices);
		data.putSerializable(getString(R.string.intent_person), person);
		toPrologRelation.putExtras(data);
	}
}