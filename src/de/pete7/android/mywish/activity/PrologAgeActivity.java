package de.pete7.android.mywish.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import de.pete7.android.mywish.R;
import de.pete7.android.mywish.adapter.PrologListAdapter;
import de.pete7.android.mywish.model.Group;
import de.pete7.android.mywish.model.GroupList;
import de.pete7.android.mywish.model.Person;
import de.pete7.android.mywish.model.Tag;
import de.pete7.android.mywish.model.TagList;
import de.pete7.android.mywish.utility.StringUtility;

public class PrologAgeActivity extends Activity {

	private GroupList groups;
	private Group age;
	private TagList choices;
	private Person person;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prolog_age);
		fetchDataFromIntent();
		prepareActionBar();
		setActiveGroup();
		prepareList();
	}

	private void fetchDataFromIntent() {
		Bundle fromPrologGender = getIntent().getExtras();
		groups = (GroupList) fromPrologGender.getSerializable(getString(R.string.intent_groups));
		choices = (TagList) fromPrologGender.getSerializable(getString(R.string.intent_choices));
		person = (Person) fromPrologGender.getSerializable(getString(R.string.intent_person));
	}

	private void setActiveGroup() {
		age = groups.getGroupById(getString(R.string.group_id_age));
	}

	private void prepareActionBar() {
		TextView title = (TextView) findViewById(R.id.action_bar_title);
		String titleString = StringUtility.buildQuestionWithGender(this, R.string.prolog_age_title, person.isMale());
		title.setText(titleString);
	}

	private void prepareList() {
		ArrayList<Tag> tags = age.getTagList().getAllTags();
		PrologListAdapter adapter = new PrologListAdapter(this, R.layout.prolog_list_item, R.id.prolog_list_item_label,
				tags);
		ListView list = (ListView) findViewById(R.id.prolog_age_list);
		list.setAdapter(adapter);
		prepareOnClickItem(list);
	}

	private void prepareOnClickItem(ListView listView) {
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int index, long id) {
				addItemToChoiceList(index);
				gotoPrologRelation();
			}
		});
	}

	private void addItemToChoiceList(long index) {
		Tag tag = age.getTagList().getTagByIndex((int) index);
		choices.addTag(tag);
	}

	private void gotoPrologRelation() {
		Intent toPrologRelation = new Intent(this, PrologRelationActivity.class);
		addDataToIntent(toPrologRelation);
		toPrologRelation.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		startActivity(toPrologRelation);
		finish();
		overridePendingTransition(0, 0);
	}

	private void addDataToIntent(Intent toPrologRelation) {
		Bundle data = new Bundle();
		data.putSerializable(getString(R.string.intent_groups), groups);
		data.putSerializable(getString(R.string.intent_choices), choices);
		data.putSerializable(getString(R.string.intent_person), person);
		toPrologRelation.putExtras(data);
	}
}
