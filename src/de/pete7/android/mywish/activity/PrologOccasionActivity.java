package de.pete7.android.mywish.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import de.pete7.android.mywish.R;
import de.pete7.android.mywish.adapter.PrologListAdapter;
import de.pete7.android.mywish.model.Group;
import de.pete7.android.mywish.model.GroupList;
import de.pete7.android.mywish.model.Person;
import de.pete7.android.mywish.model.Tag;
import de.pete7.android.mywish.model.TagList;

public class PrologOccasionActivity extends Activity {

	private GroupList groups;
	private Group occasion;
	private TagList choices;
	private Person person;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prolog_occasion);
		fetchDataFromIntent();
		prepareActionBar();
		setActiveGroup();
		prepareList();
	}

	private void fetchDataFromIntent() {
		Bundle fromPrologRelation = getIntent().getExtras();
		groups = (GroupList) fromPrologRelation.getSerializable(getString(R.string.intent_groups));
		choices = (TagList) fromPrologRelation.getSerializable(getString(R.string.intent_choices));
		person = (Person) fromPrologRelation.getSerializable(getString(R.string.intent_person));
	}

	private void prepareActionBar() {
		TextView title = (TextView) findViewById(R.id.action_bar_title);
		title.setText(R.string.prolog_occasion_title);
	}

	private void setActiveGroup() {
		occasion = groups.getGroupById(getString(R.string.group_id_occasion));
	}

	private void prepareList() {
		ArrayList<Tag> tags = occasion.getTagList().getAllTags();
		PrologListAdapter adapter = new PrologListAdapter(this, R.layout.prolog_list_item, R.id.prolog_list_item_label,
				tags);
		ListView list = (ListView) findViewById(R.id.prolog_occasion_list);
		list.setAdapter(adapter);
		prepareOnClickItem(list);
	}

	private void prepareOnClickItem(ListView listView) {
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int index, long id) {
				addItemToChoiceList(index);
				gotoCategoryList();
			}
		});
	}

	private void addItemToChoiceList(long index) {
		Tag tag = occasion.getTagList().getTagByIndex((int) index);
		choices.addTag(tag);
	}

	private void gotoCategoryList() {
		Intent toCategoryList = new Intent(this, CategoryListActivity.class);
		addDataToIntent(toCategoryList);
		startActivityForResult(toCategoryList, 0);
		finish();
	}

	private void addDataToIntent(Intent toPrologRelation) {
		Bundle data = new Bundle();
		data.putSerializable(getString(R.string.intent_groups), groups);
		data.putSerializable(getString(R.string.intent_choices), choices);
		data.putSerializable(getString(R.string.intent_person), person);
		toPrologRelation.putExtras(data);
	}
}