package de.pete7.android.mywish.builder;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import de.pete7.android.mywish.model.GroupList;

public class GroupParserTest {

	private GroupParser groupParser;

	@Before
	public void setUp() {
		groupParser = new GroupParser();
	}

	@Test
	public void parseJson_emptyString_emptyGroupList() {
		GroupList result = groupParser.parseJson("");
		
		//hamcrest:
		assertThat(result.getList(), is(empty());
		
		//assertJ:
		assertThat(result.getList()).isEmpty();
	}
}
